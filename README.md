
# dnslines

Tool used to maintain the following kind of documentational DNS zone.

    $ dig +short -t txt rtfm.arrakis.se | sort
    "audiorecode"
    "conntrack"
    "gs-dynspace"
    "gs-numbers"
    "gs-tabs"
    "ssh-multiplex"
    "ssh-proxy"
    "tsig"
    $ dig +short -t txt gs-tabs.rtfm.arrakis.se
    "gsettings set org.gnome.shell.app-switcher current-workspace-only true"
    $ dig +short -t txt tsig.rtfm.arrakis.se
    "dnssec-keygen -a hmac-sha256 -b 256 -n HOST host1-host2"
    $

It's built around doing Dynamic DNS updates, and uses [dnspython][1]
under the hood.

See also https://blog.bogosity.se/2016/11/06/oneliners-in-dns/.


## Configuration

To use _dnslines_ you need to setup a dedicated dynamic zone. You will
also need to configure a TSIG key with the permissions to do updates
as well as (preferably) zone transfers.

Client side all configuration come in the form of environment variables.

### Required config

* *DNSLINES_ALG*: Algorithm used by the TSIG key
* *DNSLINES_NAME*: Name of the TSIG key
* *DNSLINES_SECRET*: The actual TSIG key
* *DNSLINES_ZONE*: Name of the zone to maintain.

### Optional config

* *DNSLINES_SERVER*: DNS Server to update against. Defaults to the SOA MNAME.
* *DNSLINES_TTL*: TTL value to set. Defaults to 300.

### Example config

    export DNSLINES_ALG="hmac-sha256"
    export DNSLINES_NAME="foo-bar"
    export DNSLINES_SECRET="DNSS8AI9rkRaKGEcE/f70hN21zSebSPWe0hCU295LjA="
    export DNSLINES_SERVER="ns-master.example.net"
    export DNSLINES_ZONE="dnsdocs.example.net"


## Usage

    $ dnslines --help
    Usage:
      dnslines --add <name> <oneliner>
      dnslines --set <name> <oneliner>
      dnslines --delete <name>
      dnslines --reindex
      dnslines --help

    options:
     -h, --help
        Prints this help message.
     -a, --add <name> <oneliner>
        Adds a new oneliner. Will fail if name already exists.
     -s, --set <name> <oneliner>
        Sets a oneliner. Will overwrite if name already exists.
     -d, --delete <name>
        Deletes a oneliner.
     --reindex
        Rebuilds the index. Ideally never needed.


## License

dnslines is available under the [MIT license][2].


[1]: http://www.dnspython.org/
[2]: https://opensource.org/licenses/MIT
